def print_matrix(matrix):  # fungsi print matrix
    for line in matrix:  # iterasi matrix
        print("  ".join(map(str, line)))


def transpose(matrix):  # Fungsi determinan
    baris = len(matrix)  # panjang baris
    kolom = len(matrix[0])  # panjang kolom
    mat_transpose = []
    for j in range(kolom):  # iterasi kolom
        row = []
        for i in range(baris):  # iterasi baris
            row.append(matrix[i][j])  # transpose
        mat_transpose.append(row)  # menambahkan ke list baru
    return mat_transpose  # return hasil


def determinan(matrix):  # fungsi determinan
    operation = matrix[0][0] * matrix[1][1] - matrix[0][1] * \
        matrix[1][0]  # penerapan rumus determinan
    return operation


def tambah(matrix_a, matrix_b):  # fungsi penjumlahan
    baris = len(matrix_a[0])
    kolom = len(matrix_a)
    mat_hasil = []
    for j in range(kolom):
        row = []
        for i in range(baris):
            print(j, i)
            # jumlahkan sesama posisi matrix
            jumlah = matrix_a[j][i] + matrix_b[j][i]
            row.append(jumlah)
        mat_hasil.append(row)
    return mat_hasil  # return hasil penjumlahan


def kurang(matrix_a, matrix_b):  # fungsi pengurangan
    baris = len(matrix_a[0])
    kolom = len(matrix_a)
    mat_hasil = []
    for j in range(kolom):
        row = []
        for i in range(baris):
            # kurangkan matrix sesuai posisi
            jumlah = matrix_a[j][i] - matrix_b[j][i]
            row.append(jumlah)
        mat_hasil.append(row)
    return mat_hasil  # return hasil


while(True):  # perulangan kalkulator
    print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang dapat dilakukan:")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Transpose")
    print("4. Determinan")
    print("5. Keluar")

    operasi = input("Silakan pilih operasi: ")

    if operasi.isdigit():  # cek input digit
        operasi_int = int(operasi)  # ubah ke int
        if operasi_int > 0 and operasi_int <= 5:  # validasi input

            if operasi_int > 0 and operasi_int <= 4:  # validasi input
                ukuran_x = 2
                ukuran_y = 2
                matrix_x = list()
                matrix_y = list()
                valid_input = True

                if operasi_int != 4:  # jika bukan det
                    ukuran = input("\nUkuran Matriks: ")

                    list_ukuran = ukuran.split()
                    # print(list_ukuran)

                    if len(list_ukuran) == 3 and list_ukuran[0].isdigit() \
                            and list_ukuran[1] == 'x' and list_ukuran[2].isdigit():
                        ukuran_x = int(list_ukuran[0])
                        ukuran_y = int(list_ukuran[2])
                    else:
                        print("Terjadi kesalahan input. Silakan ulang kembali.\n")
                        continue

                print()
                if operasi_int == 3 or operasi_int == 4:
                    for x in range(ukuran_x):
                        text_input = 'Baris ' + str(x + 1) + ' matrix: '
                        input_nilai = input(text_input)
                        list_nilai = input_nilai.split()
                        int_list = [int(i) for i in list_nilai]
                        if len(int_list) != ukuran_y:
                            valid_input = False
                            break
                        matrix_x.append(int_list)
                else:
                    for x in range(ukuran_x):
                        text_input = 'Baris ' + str(x + 1) + ' matrix 1: '
                        input_nilai = input(text_input)
                        list_nilai = input_nilai.split()
                        int_list = [int(i) for i in list_nilai]
                        if len(int_list) != ukuran_y:
                            valid_input = False
                            break
                        matrix_x.append(int_list)

                    print()

                    for x in range(ukuran_x):
                        text_input = 'Baris ' + str(x + 1) + ' matrix 2: '
                        input_nilai = input(text_input)
                        list_nilai = input_nilai.split()
                        int_list = [int(i) for i in list_nilai]
                        if len(int_list) != ukuran_y:
                            valid_input = False
                            break
                        matrix_y.append(int_list)

                # print(matrix_x, matrix_y)
                if valid_input:  # validasi input
                    list_hasil = list()
                    hasil_deteminan = 0
                    if operasi_int == 1:  # jika penjumlahan
                        list_hasil = tambah(matrix_x, matrix_y)
                    elif operasi_int == 2:  # jika pengurangan
                        list_hasil = kurang(matrix_x, matrix_y)
                    elif operasi_int == 3:  # jika tranpose
                        list_hasil = transpose(matrix_x)
                    elif operasi_int == 4:  # jika determinan
                        hasil_deteminan = determinan(matrix_x)

                    print("\nHasil dari operasi:")
                    if operasi_int == 4:
                        print(hasil_deteminan)  # print hasil determinan
                    else:
                        print_matrix(list_hasil)  # print hasil matrix
                    print()
                else:
                    print("Terjadi kesalahan input. Silakan ulang kembali.\n")
                    continue
            else:
                print("\nSampai Jumpa!")
                break
        else:
            print("operasi yang anda masukkan tidak valid\n")
    else:
        print("operasi yang anda masukkan tidak valid\n")
