#Inisilisai Variable
hp_el = 100
hp_raiden = 100
putaran = 1
ultimate_skill = True

while(hp_el > 0 and hp_raiden > 0) : #Perulangan untuk menentukan pertandingan akan berlanjut
    print("Hp Ei: ", hp_el)
    print("Hp Raiden: ", hp_raiden)
    print("Putaran ke ", putaran)
    print("------------------------------")
    if (putaran % 3 ==0): # Jika Raiden akan menggunakan ultimate skill
        print("Raiden bersiap menggunakan  \"The Final Calamity\"")
    print("Apa yang ingin anda lakukan?")
    print("1. Menyerang")
    print("2. Menghindar")
    if (ultimate_skill): # Jika Ei belum menggunakan ultimate skill
        print("3. Musou no Hitotachi")

    serangan_el = input("Masukkan pilihan anda: ") # Input serangan

    # Serangan Ei
    if (serangan_el == "1"):
        print("EI menyerang Raiden dan mengurangi Hp Raiden sebesar 20")
        hp_raiden -= 20
    elif (serangan_el == "3"):
        print("EI menyerang Raiden dengan \"Musou no Hitotachi\" dan mengurangi Hp Raiden sebesar 50")
        hp_raiden -= 50
        ultimate_skill = False
    
    # Serangan Raiden
    if (serangan_el == "2"): #Raidan tidak bisa menyerang karena Ei memilih menghindar
        print("Raiden menyerang namun Ei berhasil menghindar")
    else: #Raiden bisa menyerang karena Ei tidak menghindar
        if (putaran % 3 ==0):
            print("Raiden menyerang Ei dengan \"The Final Calamity\" dan mengurangi Hp Ei sebesar 50")
            hp_el -= 50
        else:
            print("Raiden menyerang Ei dan mengurangi Hp Ei sebesar 20")
            hp_el -= 20

    print("------------------------------")
    putaran += 1 #Iterasi putaran

# Hasil simulasi pertandingan
if (hp_el <= 0 and hp_raiden <= 0):
    print("Pertarungan Seri")
elif (hp_raiden <= 0):
    print("Ei memenangkan pertandingan")
else:
    print("Raiden memenangkan pertandingan")