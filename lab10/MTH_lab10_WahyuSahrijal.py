from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename
from tkinter.scrolledtext import *

# TODO: Lengkapi class Application dibawah ini
class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()

    def initUI(self):
        # TODO: Atur judul dan ukuran dari main window,
        # lalu buat sebuah Frame sebagai anchor dari seluruh button
        self.master.title("Pacil Editor")
        self.master.geometry("600x400")
        self.frame = Frame(self.master)
        self.frame.pack()
        self.editorframe = Frame(self.master)
        self.editorframe.pack()
        pass

    def create_buttons(self):
        # TODO: Implementasikan semua button yang dibutuhkan
        button1 = Button(self.frame, text='Open File')
        button1.pack(side = LEFT)
        button1.bind('<Button-1>', self.load_file_event)
        button2 = Button(self.frame, text='Save File')
        button2.pack(side = LEFT)
        button2.bind('<Button-1>', self.save_file_event)
        button3 = Button(self.frame, text='Quit Program', command=self.master.destroy)
        button3.pack(side = LEFT)

    def create_editor(self):
        # TODO: Implementasikan textbox
        self.edit = ScrolledText(self.editorframe)
        self.edit.pack(fill=BOTH, expand=True)
        pass

    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        # TODO: tampilkan result di textbox
        self.set_text(result)
        

    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        # TODO: ambil isi dari textbox lalu simpan dalam file dengan nama file_name
        file_saves = open(file_name, 'w')
        file_saves.write(self.get_text())
        file_saves.close()
    
    def set_text(self, text=''):
        self.edit.delete('1.0', END)
        self.edit.insert('1.0', text)
        self.edit.mark_set(INSERT, '1.0')
        self.edit.focus()

    def get_text(self):
        return self.edit.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()
