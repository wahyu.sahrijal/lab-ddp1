print("Masukkan pesan Enkrispi: ")  # input pesan rahasia
input_pesan_rahasia = input()

# lacak posisi key
first = input_pesan_rahasia.find('*')
sec = input_pesan_rahasia.find('*', first+1) + 1

get_code = int(input_pesan_rahasia[first+1:sec-1])  # mengambil key
nilai_n = 20220310 - get_code #Mencari nilai n

#Menghapus key dari text agar tidak menggangu ketiak proses screening
target = input_pesan_rahasia[first:sec] 
input_pesan_rahasia = input_pesan_rahasia.replace(target, "")

list_pesan_rahasia = list(input_pesan_rahasia)  # ubah text menjadi list
list_pesan_asli = "" #Variable output

for pesan_rahasia in list_pesan_rahasia:  # iterasi mencari pesan rahasia
    if pesan_rahasia.isupper():  # mencari uppercase
        lower = pesan_rahasia.lower()
        orde = ord(lower) - int(nilai_n)  # ekstraksi characrter
        while orde < 97:
            orde += 26
        list_pesan_asli += chr(orde)
    elif pesan_rahasia.isdigit():  # mencari angka
        angka = int(pesan_rahasia)  # ekstraksi angka
        angka = (int(nilai_n) * 2) - ((int(nilai_n) * 2) - angka)
        string_angka = str(angka)
        list_pesan_asli += string_angka
    elif pesan_rahasia == '$':  # ekstraksi spasi
        list_pesan_asli += ' '

#Output hasil deskripsi
print(list_pesan_asli)
