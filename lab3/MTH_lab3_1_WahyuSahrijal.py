import random  # Import libary random untuk kebutuhan random key di paragraf

# variabel paragraf untuk enkripsi
paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "
key = 20220310  # variable key

# text input
print('Masukkan pesan yang akan dikirim:')
input_pesan = input()
input_n = input('Pilih nilai n: ')

input_pesan = input_pesan.lower()  # Lowercase text
list_pesan = list(input_pesan)  # ubah text menjadi list

i = 0
# iterasi pesan
for pesan in list_pesan:
    if pesan.isspace():  # cek dan ganti spasi
        list_pesan[i] = '$'
    elif pesan.isdigit():  # check angka dan ubah angka
        angka = int(list_pesan[i])
        angka += (int(input_n) * 2)
        string_angka = str(angka)
        list_pesan[i] = string_angka[-1]
    else:  # cek dan ubah char
        orde = ord(list_pesan[i]) + int(input_n)
        while orde > 122:
            orde -= 26
        list_pesan[i] = chr(orde)
    i += 1

key -= int(input_n)  # Menentukan key berdasarkan nilai n
# Membuat format key untuk di taruh di paragraf
format_key = '*' + str(key) + '*'

# melakukan generate posistion
random_position = random.randint(0, len(paragraf))

finding = 0  # iterasi index pada kata
for pesan in list_pesan:  # memasukkan pesan rahasia
    if pesan == '$' or pesan.isdigit():  # memasukkan angka dan spasi
        finding += 1
        paragraf = paragraf[:finding]+str(pesan)+paragraf[finding:]
    else:  # memasukkan teks
        find = paragraf.find(pesan)
        while find < finding:
            find = paragraf.find(pesan, find+1)
        paragraf = paragraf[:find]+pesan.upper()+paragraf[find+1:]
        finding = find

paragraf = paragraf[:random_position] + \
    str(format_key)+paragraf[random_position:]  # memasukkan key ke paragraf

# Output hasil paragraf
print(paragraf)
