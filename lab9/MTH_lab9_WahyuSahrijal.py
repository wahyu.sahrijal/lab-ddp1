from datetime import datetime


class Karyawan:  # Class Parent
    def __init__(self, username, password, nama, umur, role):
        self.__username = username
        self.__password = password
        self.__nama = nama
        self.__umur = umur
        self.__role = role
        self.__last_login = None

    def set_last_login(self, last_login):
        self.__last_login = last_login
    # Getter

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def get_nama(self):
        return self.__nama

    def get_umur(self):
        return self.__umur

    def get_role(self):
        return self.__role

    def get_last_login(self):
        return self.__last_login

    def check_account(self, username, password):
        return self.__username == username and self.__password == password

    def __str__(self):  # Method Print
        return """Username : {} \nNama : {} \nUmur : {}\nLogin Terakhir : {} \nRole : {} \n""".format(self.__username, self.__nama,
                                                                                                      self.__umur, self.__last_login,
                                                                                                      self.__role)


class Chef(Karyawan):  # Child class chef
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.__buat = 0
        self.__buang = 0
    # setter

    def set_buang(self, buang):
        self.__buang += buang

    def set_buat(self, buat):
        self.__buat += buat
    # getter chef

    def get_buat(self):
        return self.__buat

    def get_buang(self):
        return self.__buang

    def __str__(self):  # Method print chef
        return super().__str__() + """Jumlah kue yang telah dibuat: {}\nJumlah kue yang telah dibuang: {}""".format(self.__buat, self.__buang)


class Janitor(Karyawan):  # child class janitor
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.__bersih = 0
        self.__beli_detergen = 0
    # setter janitor

    def set_bersih(self, bersih):
        self.__bersih += bersih

    def set_beli_detergen(self, beli):
        self.__beli_detergen += beli
    # getter janitor

    def get_bersih(self):
        return self.__bersih

    def get_beli_detergen(self):
        return self.__beli_detergen

    def __str__(self):  # Method janitor
        return super().__str__() + """Jumlah berapa kali membersihkan toko: {}\nJumlah deterjen yang telah dibeli: {}""".format(self.__bersih, self.__beli_detergen)


class Kasir(Karyawan):  # child class kasir
    def __init__(self, username, password, nama, umur, role):
        super().__init__(username, password, nama, umur, role)
        self.__pemasukkan = 0

    def set_pemasukkan(self, pemasukkan):
        self.__pemasukkan += pemasukkan
    # getter kasir

    def get_pemasukkan(self):
        return self.__pemasukkan

    def __str__(self):  # method kasir
        return super().__str__() + "Jumlah uang diterima: {}".format(self.__pemasukkan)


def check_data_karyawan(list_karyawan, username):  # fungsi mengecek data karyawan
    if len(list_karyawan) < 1:  # rekursi
        return True
    else:
        if list_karyawan[0].get_username() != username:  # cek list username
            return check_data_karyawan(list_karyawan[1:], username)
        else:
            return False


def cetak_perintah1(list_karyawan):  # fungsi cetak perintah 1
    karyawan = None

    print("Format data: [username] [password] [nama] [umur] [role]")
    input_data_karyawan = input("Input data karyawan baru: ")
    if len(input_data_karyawan.split()) == 5:  # cek data list
        username = input_data_karyawan.split()[0]
        if check_data_karyawan(list_karyawan, username):  # cek karyawan terdaftar
            password = input_data_karyawan.split()[1]
            nama = input_data_karyawan.split()[2]
            umur = input_data_karyawan.split()[3]
            role = input_data_karyawan.split()[4]

            if role.lower() == "kasir":  # filter by role
                karyawan = Kasir(username, password, nama, umur, role)
                print("Karyawan", nama, "berhasil ditambahkan")
            elif role.lower() == "janitor":
                karyawan = Janitor(username, password, nama, umur, role)
                print("Karyawan", nama, "berhasil ditambahkan")
            elif role.lower() == "chef":
                karyawan = Chef(username, password, nama, umur, role)
                print("Karyawan", nama, "berhasil ditambahkan")
            else:
                print("Role tidak ditemukan")
        else:
            print("Username telah terdaftar")
    else:
        print("Format data yang kamu masukkan salah")

    return karyawan


def cetak_perintah2(list_karyawan):  # cetak perintah 2
    login = False
    data_karyawan = None
    username = input("Username: ")
    password = input("Password: ")

    for karyawan in list_karyawan:  # iterasi list karyawan
        if karyawan.check_account(username, password):  # cek data karyawan

            karyawan.set_last_login(datetime.now())  # set login time
            login = True
            data_karyawan = karyawan
            break

    if login:
        print("Selamat datang", data_karyawan.get_nama())
    else:
        print("username/password salah. Silahkan coba lagi")

    return data_karyawan


def update_list_karyawan(list_karyawan, karyawan):  # fungsi update list karyawan
    new_data = []
    for kry in list_karyawan:  # iterasi list karyawan
        if kry.get_username() == karyawan.get_username():  # cek username
            new_data.append(karyawan)  # update data
        else:
            new_data.append(kry)
    return new_data


def cetak_perintah3(data_karyawan, jml_cash):  # perintah 3
    input_pembayaran = input("Jumlah pembayaran: ")

    if input_pembayaran.isdigit():  # cek input int
        data_karyawan.set_pemasukkan(int(input_pembayaran))  # set pemasukkan
        jml_cash += int(input_pembayaran)
        print("Berhasil menerima uang sebanyak", input_pembayaran)
    else:
        print("Input pembayaran belum sesuai")

    return data_karyawan, jml_cash


def cetak_perintah4(data_karyawan, jml_detergen, bersih):  # perintah 4
    if jml_detergen > 0:  # cek detergen
        data_karyawan.set_bersih(1)  # Set bersih
        print("Toko berhasil dibersihkan")
        jml_detergen -= 1
        bersih = datetime.now()  # update bersih
    else:
        print("Diperlukan setidaknya 1 detergen untuk membersihkan Toko")
    return data_karyawan, jml_detergen, bersih


def cetak_perintah5(data_karyawan, jml_detergen):  # perintah 5
    input_beli = input("Jumlah pembelian deterjen: ")
    if input_beli.isdigit():  # cek input int
        data_karyawan.set_beli_detergen(int(input_beli))
        jml_detergen += int(input_beli)
        print("Berhasil membeli {} detergen".format(input_beli))
    else:
        print("Input buat belum sesuai")
    return data_karyawan, jml_detergen


def cetak_perintah6(data_karyawan, jml_kue):  # perintah 6
    input_buat = input("Buat Berapa Kue?: ")

    if input_buat.isdigit():  # cek input int
        data_karyawan.set_buat(int(input_buat))
        jml_kue += int(input_buat)
        print("Berhasil membuat {} kue".format(input_buat))
    else:
        print("Input buat belum sesuai")
    return data_karyawan, jml_kue


def cetak_perintah7(data_karyawan, jml_kue):  # perintah 7
    input_buang = input("Buang Berapa Kue?: ")

    if input_buang.isdigit():  # cek input digit
        if jml_kue >= int(input_buang):  # cek stok kue
            data_karyawan.set_buang(int(input_buang))
            print("Berhasil membuang {} kue".format(input_buang))
            jml_kue -= int(input_buang)
        else:
            print(
                "Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")
    else:
        print("Input buat belum sesuai")
    return data_karyawan, jml_kue


def cetak_perintah8(karyawan, cash, kue, deterjen, bersih):  # fungsi cetak perintah 8
    print("=====================================")
    print("STATUS TOKO HAMURA SAAT INI")
    print("Jumlah Karyawan: ", karyawan)
    print("Jumlah Cash: ", cash)
    print("Jumlah Kue: ", kue)
    print("Jumlah Deterjen: ", deterjen)
    print("Terakhir kali dibersihkan: ", bersih)
    print("=====================================")


def cetak_perintah9(list_data):  # fungsi cetak perintah 9
    list_karyawan = list_data
    if len(list_karyawan) != 0:  # Cek Karyawan terdaftar
        for karyawan in list_karyawan:
            print("===============================")
            print(karyawan)
            print("===============================")
    else:
        print("Belum ada karyawan")


def cetak_daftar_perintah(role):  # cetak daftar perintah
    input_perintah = ""
    if role.lower() == "kasir":  # filter by role input
        input_perintah = cetak_daftar_perintah_kasir()
    elif role.lower() == "janitor":
        input_perintah = cetak_daftar_perintah_janitor()
    elif role.lower() == "chef":
        input_perintah = cetak_daftar_perintah_chef()
    else:
        input_perintah = cetak_daftar_perintah_user()

    return input_perintah


def cetak_daftar_perintah_user():  # cetak sebelum login
    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("1. Register karyawan baru")
    print("2. Login")
    print("8. Status Report")
    print("9. Karyawan Report")
    print("11. Exit\n")
    return input("Pilihan: ")


def cetak_daftar_perintah_kasir():  # cetak perintah sebagai kasir
    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("3. Terima pembayaran")
    print("10. Logout\n")
    return input("Pilihan: ")


def cetak_daftar_perintah_janitor():  # cetak perintah sebagai janitor
    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("4. Bersihkan toko")
    print("5. Beli deterjen")
    print("10. Logout\n")
    return input("Pilihan: ")


def cetak_daftar_perintah_chef():  # cetak perintah sebagai chef
    print("\nApa yang ingin anda lakukan? (Tulis angka saja)")
    print("6. Buat kue")
    print("7. Buang kue")
    print("10. Logout\n")
    return input("Pilihan: ")


def main():
    print("Selamat datang di Sistem Manajemen Homura")
    cek_exit = False
    jml_cash = 0
    jml_karyawan = 0
    jml_detergen = 0
    jml_kue = 0
    bersih = None
    list_karyawan = []
    role = ""
    karyawan_login = None

    while not cek_exit:
        nomor_perintah = cetak_daftar_perintah(role)
        if nomor_perintah == "1":  # perintah 1
            if role.lower() == "":  # cek apabila ada role
                data_karyawan = cetak_perintah1(list_karyawan)
                if data_karyawan != None:  # menambahkan karyawan
                    list_karyawan.append(data_karyawan)
                    jml_karyawan += 1
            else:
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
        elif nomor_perintah == "2":  # perintah 2
            if role.lower() == "":  # cek ada role
                data_karyawan = cetak_perintah2(list_karyawan)
                if data_karyawan != None:  # cek data karyawan
                    update_list_karyawan(list_karyawan, data_karyawan)
                    karyawan_login = data_karyawan
                    role = karyawan_login.get_role()
            else:
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
        elif nomor_perintah == "3":  # perintah 3
            if role.lower() == "kasir":
                data_karyawan, total_cash = cetak_perintah3(
                    karyawan_login, jml_cash)
                update_list_karyawan(list_karyawan, data_karyawan)
                jml_cash = total_cash
            else:
                print("Anda harus login sebagai kasir untuk menjalankan fungsi ini")
        elif nomor_perintah == "4":  # perintah 4
            if role.lower() == "janitor":  # cek role
                data_karyawan, total_detergen, status_bersih = cetak_perintah4(
                    karyawan_login, jml_detergen, bersih)
                update_list_karyawan(list_karyawan, data_karyawan)
                jml_detergen = total_detergen
                bersih = status_bersih
            else:
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
        elif nomor_perintah == "5":  # perintah 5
            if role.lower() == "janitor":  # cek role
                data_karyawan, total_detergen = cetak_perintah5(
                    karyawan_login, jml_detergen)
                update_list_karyawan(list_karyawan, data_karyawan)
                jml_detergen = total_detergen
            else:
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
        elif nomor_perintah == "6":  # perintah 6
            if role.lower() == "chef":  # cek role
                data_karyawan, total_kue = cetak_perintah6(
                    karyawan_login, jml_kue)
                update_list_karyawan(list_karyawan, data_karyawan)
                jml_kue = total_kue
            else:
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
        elif nomor_perintah == "7":  # perintah 7
            if role.lower() == "chef":  # cek role
                data_karyawan, total_kue = cetak_perintah7(
                    karyawan_login, jml_kue)
                update_list_karyawan(list_karyawan, data_karyawan)
                jml_kue = total_kue
            else:
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
        elif nomor_perintah == "8":  # perintah 8
            if role.lower() == "":  # cek role
                cetak_perintah8(jml_karyawan, jml_cash,
                                jml_kue, jml_detergen, bersih)
            else:
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
        elif nomor_perintah == "9":  # perintah 9
            if role.lower() == "":  # cek ada role
                cetak_perintah9(list_karyawan)  # tampilkan perintah
            else:
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
        elif nomor_perintah == "10":  # perintah 10
            if role.lower() != "":  # cek role
                print("LOGOUT BERHASIL")
                role = ""
                karyawan_login = None
            else:
                print("Anda harus login sebelum dapat menjalankan fungsi ini")
        elif nomor_perintah == "11":  # perintah 11
            if role.lower() == "":  # cek role
                print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
                cek_exit = True
            else:
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
        else:
            print(f"Nomor masukan {nomor_perintah} tidak terdapat pada daftar")
    return


if __name__ == "__main__":
    main()
