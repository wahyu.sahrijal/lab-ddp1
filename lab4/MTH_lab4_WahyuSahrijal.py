input_nama_file = input("Masukkan nama file input: ") #input file
output_nama_file = input("Masukkan nama file output: ") #output file

try:
  input_file = open(input_nama_file,"r")#open file input dan read
  output_file = open(output_nama_file,"w")#open file output dan write

  total_baris_input = 0
  total_baris_output = 0

  for baris_file in input_file.readlines(): #membaca baris teks di dalam file
    baris_teks = baris_file.strip()
    list_baris = list(baris_teks) #convert baris teks ke bentuk list

    total_angka = 0
    kalimat = ""
    total_baris_input += 1

    for huruf in list_baris: #pengecekan character angka dan alphabet
      if huruf.isnumeric(): #cek angka
        total_angka += int(huruf) #menampung jumlah angka
      elif huruf.isalpha() or huruf.isspace(): #cek alphabet dan spasi
        kalimat += huruf #menampung char menjadi kalimat
    
    if total_angka % 2 != 0: #filter jumlah angka
      continue

    output_file.write(kalimat+'\n') #write kalimat ke dalam output file
    total_baris_output += 1

  output_file.close() #close output file
  input_file.close() #close input file
  print("\nTotal baris dalam file input:", total_baris_input) #print jumlah baris input file
  print("Total baris yang disimpan:", total_baris_output) #print jumlah baris output file
except:
  print("\nNama file yang anda masukkan tidak ditemukan :(") #error apabila file tidak ditemukan