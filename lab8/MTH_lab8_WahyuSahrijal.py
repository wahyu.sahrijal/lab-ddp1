class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping

    def get_tipe(self):
        # TODO: Implementasikan getter untuk tipe!
        return self.__tipe

    def get_harga(self):
        # TODO: Implementasikan getter untuk harga!
        return self.__harga

    def get_tulisan(self):
        # TODO: Implementasikan getter untuk tulisan!
        return self.__tulisan

    def get_angka_lilin(self):
        # TODO: Implementasikan getter untuk angka_lilin!
        return self.__angka_lilin

    def get_topping(self):
        # TODO: Implementasikan getter untuk topping!
        return self.__topping


class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga=2500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Kue Sponge", harga, tulisan, angka_lilin, topping)
        self.__rasa = rasa
        self.__warna_frosting = warna_frosting
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!

    def rasa(self):
        return self.__rasa

    def warna_frosting(self):
        return self.__warna_frosting

    def __str__(self):  # Print Object
        return "{} {}".format(self.get_tipe(), self.rasa())


class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Kue Keju", harga, tulisan, angka_lilin, topping)
        self.__jenis_kue_keju = jenis_kue_keju

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def jenis_kue_keju(self):
        return self.__jenis_kue_keju

    def __str__(self):  # Print Object
        return "{} {}".format(self.get_tipe(), self.jenis_kue_keju())


class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__("Kue Buah", harga, tulisan, angka_lilin, topping)
        self.__jenis_kue_buah = jenis_kue_buah

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def jenis_kue_buah(self):
        return self.__jenis_kue_buah

    def __str__(self):  # Print Object
        return "{} {}".format(self.get_tipe(), self.jenis_kue_buah())

# Fungsi ini harus diimplementasikan!


def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!
    print("\nJenis Kue:")
    print("1. Kue Sponge")
    print("2. Kue Keju")
    print("3. Kue Buah\n")
    jenis = input("Pilih tipe kue: ")

    custom = None
    if jenis == '1':  # Kondisi Kue Sponge
        jenis_kue = input("Pilih rasa kue Sponge (Chocholate/Strawberry): ")
        warna = input("Pilih Warna Frosting (Kuning/Hijau/Merah)")
        topping_kue = input("Pilih topping (Ceri/Stroberi): ")
        lilin = input("Angka lilin yang anda pilih adalah : ")
        tulisan = input("Tulisan ucapan yang anda inginkan adalah : ")
        custom = KueSponge(tulisan, lilin, topping_kue, jenis_kue, warna)
    elif jenis == '2':  # Kondisi Kue Keju
        jenis_kue = input("Pilih jenis kue Keju  (New York/Ricotta/Pumpkin): ")
        topping_kue = input("Pilih topping (Ceri/Stroberi): ")
        lilin = input("Angka lilin yang anda pilih adalah : ")
        tulisan = input("Tulisan ucapan yang anda inginkan adalah : ")
        custom = KueKeju(tulisan, lilin, topping_kue, jenis_kue)
    elif jenis == '3':  # Kondisi Kue Buah
        jenis_kue = input("Pilih jenis kue Buah (American/British): ")
        topping_kue = input("Pilih topping (Apple/Grape/Melon/Mix): ")
        lilin = input("Angka lilin yang anda pilih adalah : ")
        tulisan = input("Tulisan ucapan yang anda inginkan adalah : ")
        custom = KueBuah(tulisan, lilin, topping_kue, jenis_kue)
    return custom  # return object sesuai kondisi

# Fungsi ini harus diimplementasikan!


def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    print("\nPilihan paket istimewa:")
    print("1. New York-style Cheesecake with Strawberry Topping")
    print("2. Chocolate Sponge Cake with Cherry Topping and Blue Icing")
    print("3. American Fruitcake with Apple-Grape-Melon-mix Topping\n")

    input_paket = input("Pilih paket: ")
    input_angka_lilin = input("Masukkan angka lilin: ")
    input_tulisan = input("Masukkan tulisan pada kue: ")

    paket = None
    if input_paket == "1":  # Kondisi paket 1
        paket = KueKeju(input_tulisan, input_angka_lilin,
                        "Strawberry", "New York")
    elif input_paket == "2":  # Kondisi paket 2
        paket = KueSponge(input_tulisan, input_angka_lilin,
                          "Cherry and Blue Icing", "Chocolate", "Chocolate")
    elif input_paket == "3":  # Kondisi paket 3
        paket = KueBuah(input_tulisan, input_angka_lilin,
                        "Apple-Grape-Melon-mix", "American")
    return paket

# Fungsi ini harus diimplementasikan!


def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    print(kue, "American dengan topping", kue.get_topping())
    print("Tulisan ucapan yang anda inginkan adalah \"" + kue.get_tulisan() + "\"")
    print("Angka lilin yang anda pilih adalah", kue.get_angka_lilin())
    print("Harga: ¥", kue.get_harga())
    print("")

# Fungsi main jangan diubah!


def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")

        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input(
                "Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input(
                    "Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

    print("\nTerima kasih sudah berbelanja di Homura!")


if __name__ == "__main__":
    main()
