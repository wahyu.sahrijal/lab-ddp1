# Fungsi mengurutkan
def sort_list_lomba(e):
    return e['nama']

print("==================================")#Pesan Welcome
print(" WELCOME TO KARTI-NIAN CHALLENGE. ")
print("==================================\n")

input_total_lomba = input("Masukkan jumlah acara yang akan dilombakan: ")#Input jumlah Lomba
total_lomba = int(input_total_lomba)

list_lomba = list()

for i in range(total_lomba):  # looping input lomba
    print("\nLOMBA", (i+1))
    input_nama_lomba = input("Nama Lomba: ")
    input_juara1 = input("Hadiah juara 1: ")
    input_juara2 = input("Hadiah juara 2: ")
    input_juara3 = input("Hadiah juara 3: ")
    list_lomba.append({"nama": input_nama_lomba, "juara1": int(input_juara1),
                       "juara2": int(input_juara2), "juara3": int(input_juara3)})  # Dict lomba

list_lomba.sort(key=sort_list_lomba) #Proses sorting lomba by nama

print("\n==================================")#Menampilkan data lomba
print("    DATA MATA LOMBA DAN HADIAH.   ")
print("==================================")

for lomba in list_lomba:  # Iterasi output lomba
    print("\nLomba", lomba["nama"])
    print("[Juara 1]", lomba["juara1"])
    print("[Juara 2]", lomba["juara2"])
    print("[Juara 3]", lomba["juara3"])

print("\n==================================")# Pesan input data peserta
print("       DATA PESERTA CHALLENGE.    ")
print("==================================")

list_peserta = set()

input_jumlah_peserta = input("\nMasukkan Jumlah Peserta yang berpartisipasi: ")
jumlah_peserta = int(input_jumlah_peserta)
print()

for i in range(jumlah_peserta):  # iterasu input peserta
    input_peserta = input("Masukkan Nama Peserta {} : " .format(i+1))
    if not list_peserta:  # cek set kosong
        list_peserta.add(input_peserta)
    # cek nama peserta tidak ada didalam set
    elif input_peserta.lower() not in [x.lower() for x in list_peserta]:
        list_peserta.add(input_peserta)
    # cek nama peserta jika ada didalam set
    elif input_peserta.lower() in [x.lower() for x in list_peserta]:
        for i in range(len(list_peserta)):
            temp = list(list_peserta)  # convert set ke list
            if temp[i].lower() == input_peserta.lower():  # mencari nama yang sama
                list_peserta.remove(temp[i])  # hapus nama yang sama
                break
        list_peserta.add(input_peserta)  # update nama terbaru


# fungsi memasukkan daftar menang peserta
def tambah_juara_peserta(name, total, juara, list_peserta, list_lomba):
    i = 0
    for peserta in list_peserta:  # menambah daftar menang peserta
        if peserta["nama"].lower() == name.lower():  # mencari nama peserta
            list_peserta[i]["hadiah"] += total  # tambah hadiah
            list_peserta[i]["juara"].add(juara)  # tambah juara
            break
        i += 1
    return list_peserta


list_peserta_fixed = list()

for peserta in list_peserta:
    list_peserta_fixed.append({"nama": peserta, "hadiah": 0,
                               "juara": {1, 2, 3}})  # list dict peserta lomba

print("\n==================================")# Lomba dimulai
print("        CHALLENGE DIMULAI         ")
print("==================================")

for lomba in list_lomba:  # input pemenang lomba
    print("\nLomba", lomba["nama"])
    for i in range(1, 4):  # input juara
        input_peserta_juara = input("Juara {}: ".format(i))
        list_peserta_fixed = tambah_juara_peserta(input_peserta_juara, lomba["juara{}".format(i)],
                                                  i, list_peserta_fixed, list_lomba)  # menambah juara pada peserta

print("\n==================================")#Pesan Hasil final
print("           FINAL RESULT          ")
print("==================================")

list_peserta_fixed.sort(key=sort_list_lomba)  # sort juara

for peserta in list_peserta_fixed:  # menampilkan hasil akhir lomba
    print("")
    print(peserta["nama"])
    print("Total hadiah:", peserta["hadiah"])
    print("Juara yang pernah diraih:", ','.join(str(v)
          for v in peserta["juara"]))